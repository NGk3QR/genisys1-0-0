<?php

namespace pocketmine\block;
//use pocketmine\inventory\BeaconInventory;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\item\Tool;
class Beacon extends Transparent implements SolidLight{
	protected $id = self::BEACON;
	public function __construct($meta = 0){
		$this->meta = $meta;
	}
	/*public function canBeActivated() : bool{
		return true;
	}*/
	public function getLightLevel(){
		return 15;
	}
	public function getHardness(){
		return 3;
	}
	public function getName(){
        return "Beacon";
	}
	/*public function onActivate(Item $item, Player $player = null){
		if($player instanceof Player){
			$player->addWindow(new BeaconInventory($this));
		}
		return true;
	}*/
}